import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;

public class exam_01 {
    static public void main(String args[]){
        try {
            BufferedReader reader = new BufferedReader(new FileReader("input.txt"));
            String line = null; 
            while ((line = reader.readLine()) != null){
                System.out.println(line);
            }
        } catch(FileNotFoundException e1) {
            System.err.println("FileNotFoundException: " + e1.getMessage());
        } catch(IOException e2) {
            System.err.println("IOException: " + e2.getMessage());
        }
    }
}

