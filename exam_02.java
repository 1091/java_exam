import java.net.HttpURLConnection;
import java.net.URL;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class exam_02{
    static public void main(String args[]){
        try {
            HttpURLConnection connection = null;
            URL url = new URL("http://w3c.org");
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", 
                "application/x-www-form-urlencoded");
            connection.setDoInput(true);
            connection.setDoOutput(false);
            InputStream inputStream = connection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = null;
            StringBuffer response = new StringBuffer();
            while((line = bufferedReader.readLine()) != null){
                response.append(line);
                response.append('\r');
            } 
            bufferedReader.close();
            System.out.println(response.toString());
        } catch (MalformedURLException e1) {
            System.err.println("MalformedURLException: " + e1.getMessage());
        } catch (ProtocolException e2) {
            System.err.println("ProtocolException: " + e2.getMessage());
        } catch(IOException e3) {
            System.err.println("IOException: " + e3.getMessage());
        }
    }
}

